<?php

namespace App\Policies;

use App\Models\Requests;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class RequestsPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(User $user)
    {
        return $user->inPermissions('viewAnyRequests');

    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Requests  $requests
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(User $user, Requests $requests)
    {
        return $user->inPermissions('viewRequests');

    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(User $user)
    {
        return $user->inPermissions('createRequests');

    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Requests  $requests
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, Requests $requests)
    {
        return $user->inPermissions('updateRequests');

    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Requests  $requests
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(User $user, Requests $requests)
    {
        return $user->inPermissions('deleteRequests');

    }
}
