<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public function index()
    {
        // $this->authorize('viewAny', User::class);
        $user = User::where('id', ">", 0)->get();
        return response()->json($user, 200);
    }

    public function store(Request $request)
    {
        // $this->authorize('create', User::class);
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = $request->password;
        $user->phone = $request->phone;
        $user->status = $request->status;
        $user->save();
        $user->roles()->syncWithoutDetaching($request->role_id);
        return response()->json($user, 201);
    }

    public function show($id)
    {
        $user = User::where('id', $id)->first();
        // $this->authorize('view', $user);

        return response()->json($user, 200);
    }

    public function update(Request $request, $id)
    {
        $user = User::where('id', $id)->first();
        if (!$user) {
            return response()->json('It does not exist actually', 200);
        }
        // $this->authorize('update', $user);
        $user->update($request->all());
        return response()->json($user, 200);
    }


    public function destroy($id)
    {
        $user = User::where('id', $id)->first();
        if (!$user) {
            return response()->json('It does not exist actually', 200);
        }
        // $this->authorize('delete', $user);
        $user = $user->delete();

        return response()->json('Done Delete user', 200);
    }
}
