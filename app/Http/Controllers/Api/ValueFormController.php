<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\FieldAnswer;
use App\Models\ValueForm;
use Illuminate\Http\Request;

class ValueFormController extends Controller
{
    public function store(Request $request)
    {
        $valueForm = new ValueForm();
        $valueForm->form_id = $request->form_id;
        $valueForm->request_id = $request->request_id;
        $valueForm->save();
        foreach ($request->field as $item) {
            $field = new FieldAnswer();
            $field->field_id = $item["field_id"];
            $field->answer = $item["answer"];
            $field->valueForm_id = $valueForm->id;
            $field->save();
        }
        return response()->json("Done Create", 201);
    }

    public function show($id)
    {
        $valueForm = ValueForm::where('id', $id)->with('fieldAnswers')->first();
        return response()->json($valueForm, 200);
    }

    public function update(Request $request, $id)
    {
        $valueForm = ValueForm::where('id', $id)->first();
        if (!$valueForm) {
            return "null";
        }
        $valueForm->update([
            'form_id' => $request->form_id ?? $valueForm->form_id,
            'request_id' => $request->request_id ?? $valueForm->request_id
        ]);
        foreach ($request->field as $item) {
            $valueForm->fieldAnswers()->where('id', $item["id"])
                ->update([
                    'field_id' => $item["field_id"],
                    'answer' => $item["answer"]
                ]);
        }

        $valueForm = $valueForm->with('fieldAnswers')->get();

        return response()->json($valueForm, 200);

    }

}
