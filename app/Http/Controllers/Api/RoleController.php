<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Permission;
use App\Models\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{

    public function getAllPermission()
    {

        $permissions = Permission::all();
        return response()->json($permissions, 200);
    }

    public function index()
    {
        // $this->authorize('viewAny', Role::class);
        $role = Role::with('permissions')->where('id', ">", 0)->get();
        return response()->json($role, 200);
    }

    public function store(Request $request)
    {
        // $this->authorize('create', Role::class);
        $role = new Role();
        $role->name = $request->name;
        $role->description = $request->description;
        $role->save();
        $role->permissions()->syncWithoutDetaching($request->permissions_id);
        return response()->json($role, 201);
    }

    public function show($id)
    {
        $role = Role::where('id', $id)->with('permissions')->first();
        // $this->authorize('view', $role);
        return response()->json($role, 200);
    }

    public function update(Request $request, $id)
    {
        $role = Role::where('id', $id)->with('permissions')->first();
        if (!$role) {
            return response()->json('It does not exist actually', 200);
        }
        // $this->authorize('update', $role);
        $role->update([
            'name' => $request->name ?? $role->name,
            'description' => $request->description ?? $role->description
        ]);
        // $role->permissions()->syncWithoutDetaching($request->permissions_id);
        $role->permissions()->sync($request->permissions_id);
        return response()->json($role, 200);
    }


    public function destroy($id)
    {
        $role = Role::where('id', $id)->first();
        if (!$role) {
            return response()->json('It does not exist actually', 200);
        }
        // $this->authorize('delete', $role);
        $role = $role->delete();

        return response()->json('Done Delete Role', 200);
    }
}
