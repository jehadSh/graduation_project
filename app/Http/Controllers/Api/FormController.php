<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Field;
use App\Models\Form;
use Illuminate\Http\Request;

class FormController extends Controller
{

    public function index()
    {
        // $this->authorize('viewAny', Form::class);
        $form = Form::with('fields')->where('id', ">", 0)->get();
        return response()->json($form, 200);
    }


    public function store(Request $request)
    {
        // $this->authorize('create', Form::class);
        $form = new Form();
        $form->name = $request->name;
        $form->save();

        foreach ($request->field as $item) {
            $field = new Field();
            $field->type = $item["type"];
            $field->label = $item["label"];
            $field->collection_id = $item["collection_id"];
            $field->form_id = $form->id;
            $field->save();
        }

        return response()->json("Done Create", 201);
    }


    public function show($id)
    {
        $form = Form::where('id', $id)->with('fields')->first();
        // $this->authorize('view', $form);
        return response()->json($form, 200);
    }

    public function update(Request $request, $id)
    {
        $form = Form::where('id', $id)->first();
        if (!$form) {
            return response()->json('It does not exist actually', 200);
        }
        // $this->authorize('update', $form);
        $form->update([
            'name' => $request->name ?? $form->name,
        ]);
        return response()->json($form, 200);

    }


    public function destroy($id)
    {
        $form = Form::where('id', $id)->first();
        if (!$form) {
            return response()->json('It does not exist actually', 200);
        }
        // $this->authorize('delete', $form);
        $form = $form->delete();

        return response()->json('Done Delete Form', 200);
    }
}
