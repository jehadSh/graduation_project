<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Level;
use App\Models\Process;
use App\Models\Form;
use App\Models\User_Role;
use Auth;
use Illuminate\Http\Request;

class ProcessController extends Controller
{
    public function index()
    {
        // $this->authorize('viewAny', Process::class);
        $process = Process::with('levels')->where('id', ">", 0)->get();
        return response()->json($process, 200);
    }

    public function store(Request $request)
    {
        // $this->authorize('create', Process::class);
        $process = new Process();
        $process->name = $request->name;
        $process->initiator_role = $request->initiator_role;
        $process->initiator_form = $request->initiator_form;
        $process->priority = $request->priority;
        $process->save();
        $prev_level = null;
        foreach ($request->levels as $level) {
            $level_local = new Level();
            if ($level["form_id"]) {
                $level_local->form_id = $level["form_id"];
            } else {
                $level_local->comfirmable = $level["comfirmable"];
            }
            $level_local->role_id = $level["role_id"];
            $level_local->process_id = $process->id;
            $level_local->save();
            if ($prev_level != null) {
                $prev_level->next_level_id = $level_local->id;
                $prev_level->save();
            }
            $prev_level = $level_local;
            // $temp = $level_local->id;
            // $temp_level = $level_local;
        }
        return response()->json($process, 201);
    }

    public function show($id)
    {
        $process = Process::where('id', $id)

            ->with([
                'levels' => function ($query) {
                    return $query->with('form')->first();


                    // 'form' => function ($query) {
                    //     return $query->with([
                    //         'fields' => function ($query) {
                    //             return $query->with([
                    //                 'collections' => function ($query) {
                    //                     return $query->with('dataValues')->get();
                    //                 }
                    //            ])->get();
                    //         }
                    //     ])->get();
                    // }


                    // )->first();
                }
            ])


            ->first();
        // $this->authorize('view', $process);
        return response()->json($process, 200);
    }


    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        $process = Process::where('id', $id)->first();
        if (!$process) {
            return response()->json('It does not exist actually', 200);
        }
        // $this->authorize('delete', $process);
        $process = $process->delete();
        return response()->json('Done Delete process', 200);

    }

    public function getMyProcessInit()
    {
        $user = Auth::user();
        $myRole = User_Role::where('user_id', $user->id)->get()->pluck('role_id');
        $myProcess = Process::whereIn('initiator_role', $myRole)->get();

        return response()->json($myProcess, 200);
    }

    // public function getInitiatorLevel($process_id)
    // {
    //     $process = Process::where('id', $process_id)->with(
    //         [
    //             'levels' => function ($query) {
    //                 return $query->with([
    //                     'form' => function ($query) {
    //                         return $query->with([
    //                             'fields' => function ($query) {
    //                                 return $query->with([
    //                                     'collections' => function ($query) {
    //                                         return $query->with('dataValues')->get();
    //                                     }
    //                                 ])->get();
    //                             }
    //                         ])->get();
    //                     }
    //                 ])->first();
    //             }
    //         ]
    //     )->first();
    //     return response()->json($process, 200);
    // }


    public function getInitiatorLevel($process_id)
    {
        $process = Process::where('id', $process_id)
            // ->with([
            //     'levels' => function ($query) {
            //         return $query->with([
            //             'form' => function ($query) {
            //                 return $query->with([
            //                     'fields' => function ($query) {
            //                         return $query->with([
            //                             'collections' => function ($query) {
            //                                 return $query->with('dataValues')->get();
            //                             }
            //                         ])->get();
            //                     }
            //                 ])->get();
            //             }
            //         ])->first();
            //     }
            // ])
            ->first();


        $form = Form::where('id', $process->initiator_form)->with([
            'fields' => function ($query) {
                return $query->with([
                    'collections' => function ($query) {
                        return $query->with('dataValues')->get();
                    }
                ])->get();
            }
        ])->first();

        return response()->json([
            'process' => $process,
            'form' => $form
        ]);
    }
}
