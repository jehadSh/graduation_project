<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Collection;
use App\Models\DataValue;
use Illuminate\Http\Request;

class CollectionController extends Controller
{

    public function index()
    {

        $collection = Collection::where('id', ">", 0)->get();
        return response()->json($collection, 200);
    }


    public function store(Request $request)
    {
        $collection = new Collection();
        $collection->name = $request->name;
        $collection->save();

        foreach ($request->dataValues as $item) {
            $data_value = new DataValue();
            $data_value->value = $item["value"];
            $data_value->collectoin_id = $collection->id;
            $data_value->save();
        }

        return response()->json("Done Create", 201);

    }

    public function show($id)
    {
        $collection = Collection::where('id', $id)->with('dataValues')->first();
        return response()->json($collection, 200);

    }


    public function update(Request $request, $id)
    {
        $collection = Collection::where('id', $id)->with('fields')->first();
        $collection->update($request->all());

        return response()->json($collection, 200);
    }

    public function destroy($id)
    {
        $collection = Collection::where('id', $id)->delete();
        if (!$collection) {
            return response()->json('It does not exist actually', 200);
        }
        // $this->authorize('delete', $education);
        return response()->json('Done Delete Form', 200);

    }
}
