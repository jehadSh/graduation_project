<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Requests;
use App\Models\ValueForm;
use App\Models\FieldAnswer;
use App\Models\ActiveLevel;
use App\Models\Level;
use App\Models\User_Role;

use Auth;

use Illuminate\Http\Request;

class RequestController extends Controller
{
    public function index()
    {
        // $this->authorize('viewAny', Requests::class);
        $requests = Requests::where('id', ">", 0)->get();
        return response()->json($requests, 200);
    }

    public function store(Request $request)
    {
        // $this->authorize('create', Requests::class);
        $requests = new Requests();
        $requests->request_initiator = $request->request_initiator;
        $requests->request_process = $request->request_process;
        $requests->request_status = "new";
        $requests->save();

        $valueForm = new ValueForm();
        $valueForm->request_id = $requests->id;
        $valueForm->form_id = $request->form_id;
        $valueForm->save();

        $requests->initiator_form_value = $valueForm->id;
        $requests->save();

        $activeLevel = new ActiveLevel();
        $activeLevel->request_id = $requests->id;
        $level = Level::where('process_id', $request->request_process)->first();
        $activeLevel->level_id = $level->id;
        $activeLevel->save();

        $valueForm->ActiveLevel_id = $activeLevel->id;
        $valueForm->save();

        foreach ($request->field as $item) {
            $field = new FieldAnswer();
            $field->field_id = $item["field_id"];
            $field->answer = $item["answer"];
            $field->valueForm_id = $valueForm->id;
            $field->save();
        }
        return response()->json("Done Create", 201);

    }

    public function show($id)
    {
        $requests = Requests::where('id', $id)
            // ->with('activeLevel')
            ->with([
                'activeLevel' => function ($query) {
                    return $query->with([
                        'ValueFormLevel' => function ($query) {
                            return $query->with([
                                'fieldAnswers' => function ($query) {
                                    return $query->with('field')->get();
                                }
                            ])->get();
                        }
                    ]);
                }
            ])->with([
                    'valueForm' => function ($query) {
                        return $query->with([
                            'fieldAnswers' => function ($query) {
                                return $query->with('field')->get();
                            }
                        ])->get();
                    }
                ])->first();
        // $this->authorize('view', $requests);
        return response()->json($requests, 200);
    }

    public function update(Request $request, $id)
    {
        $requests = Requests::where('id', $id)->first();
        if (!$requests) {
            return response()->json('It does not exist actually', 200);
        }
        // $this->authorize('update', $requests);
        $requests->update($request->all());

        return response()->json($requests, 200);
    }
    public function destroy($id)
    {
        $requests = Requests::where('id', $id)->first();
        if (!$requests) {
            return response()->json('It does not exist actually', 200);
        }
        // $this->authorize('delete', $requests);
        $requests = $requests->delete();
        return response()->json('Done Delete Request', 200);

    }
    public function overViewRequest()
    {
        $user = Auth::user();
        $requests_new = Requests::where('request_initiator', $user->id)->where('request_status', 'new')->count();
        $requests_open = Requests::where('request_initiator', $user->id)->where('request_status', 'open')->count();
        $requests_onHold = Requests::where('request_initiator', $user->id)->where('request_status', 'onHold')->count();
        $requests_confirmed = Requests::where('request_initiator', $user->id)->where('request_status', 'confirmed')->count();
        $requests_rejected = Requests::where('request_initiator', $user->id)->where('request_status', 'rejected')->count();
        return response()->json([
            'new' => $requests_new,
            'open' => $requests_open,
            'onHold' => $requests_onHold,
            'confirmed' => $requests_confirmed,
            'rejected' => $requests_rejected,
        ]);
    }
    public function receivedRequests()
    {

        $requests = Requests::whereIn('request_status', ['new', 'open'])->whereHas('activeLevel', function ($query) {
            $query->where('is_level_complete', false)
                ->whereHas('level', function ($query) {
                    $user = Auth::user();
                    $myRole = User_Role::where('user_id', $user->id)->get()->pluck('role_id');
                    $query->whereIn('role_id', $myRole);
                });
        })->orderBy('created_at', 'desc')->get();

        return response()->json($requests, 200);
    }

    public function getRequestActiveLevel($id)
    {
        $active_level = ActiveLevel::where('request_id', $id)->where('is_level_complete', false)
            ->latest()->first();
        return $active_level;
    }

    public function activeLevelSubmit(Request $request, $ActiveLevel_id)
    {
        $active_level = ActiveLevel::where('id', $ActiveLevel_id)->first();
        $requests = Requests::where('id', $active_level->request_id)->first();

        $level = Level::where('id', $active_level->level_id)->first();
        $active_level->is_level_complete = true;
        $active_level->is_confirmed = $request->is_confirmed;

        if ($request->is_confirmed) {

            if ($level->next_level_id == null) {
                $requests->request_status = "confirmed";
            } else {
                $requests->request_status = "open";
                $next_level = Level::where('id', $level->next_level_id)->first();
                $activeLevelNext = new ActiveLevel();
                $activeLevelNext->level_id = $next_level->id;
                $activeLevelNext->request_id = $active_level->request_id;
                $activeLevelNext->save();
            }

            if ($request->form_id) {
                $valueForm = new ValueForm();
                $valueForm->request_id = $requests->id;
                $valueForm->form_id = $request->form_id;
                $valueForm->ActiveLevel_id = $active_level->id;
                $valueForm->save();
                foreach ($request->field as $item) {
                    $field = new FieldAnswer();
                    $field->field_id = $item["field_id"];
                    $field->answer = $item["answer"];
                    $field->valueForm_id = $valueForm->id;
                    $field->save();
                }
                $active_level->valueForm_id = $valueForm->id;
            }
        } else {
            $requests->request_status = "rejected";
        }
        $active_level->save();

        $requests->save();

        return response()->json("Done Successfully", 200);
    }

    public function getMyRequests()
    {
        $requests = Requests::whereHas('activeLevel', function ($query) {
            $user = Auth::user();
            $query->where('user_id', $user->id);

        })->first();

        return response()->json($requests, 200);

    }

    public function takeRequests(Request $request, $requests_id)
    {

        $active_level = ActiveLevel::where('request_id', $requests_id)->latest()->first();
        $user = Auth::user();

        if ($active_level->user_id == null) {
            $active_level->user_id = $user->id;

            $active_level->save();

            $requests = Requests::where('id', $requests_id)->first();

            $requests->request_status = 'onHold';

            $requests->save();

            return response()->json(true, 200);
        } else {
            return response()->json(false, 200);
        }

    }

    public function onHoldReceivedRequest()
    {
        $requests = Requests::where('request_status', 'onHold')->whereHas('activeLevel', function ($query) {
            $user = Auth::user();
            $query->where('is_level_complete', false)->where('user_id', $user->id);
        })->orderBy('updated_at', 'desc')->get();

        return response()->json($requests, 200);
    }

    public function rejectedReceivedRequest()
    {
        $requests = Requests::where('request_status', 'rejected')->whereHas('activeLevel', function ($query) {
            $user = Auth::user();
            $query->where('is_level_complete', true)->where('user_id', $user->id);
        })->orderBy('updated_at', 'desc')->get();

        return response()->json($requests, 200);
    }
    public function getMyRequestByStatus($status)
    {
        $user = Auth::user();
        $requests = Requests::where('request_initiator', $user->id)->where('request_status', $status)->get();
        return response()->json($requests, 200);
    }
    public function receivedRequestLevel($requests_id)
    {
        $user = Auth::user();
        $active_level = ActiveLevel::where('request_id', $requests_id)
            ->where('is_level_complete', false)->where('user_id', $user->id)
            ->with([
                'level' => function ($q) {
                    return $q->with([
                        'form' => function ($query) {
                            return $query->with([
                                'fields' => function ($query) {
                                    return $query->with([
                                        'collections' => function ($query) {
                                            return $query->with('dataValues')->get();
                                        }
                                    ])->get();
                                }
                            ])->get();
                        }
                    ])->get();
                }
            ])->latest()->first();

        return response()->json($active_level, 200);

    }

    public function myLatestRequests()
    {
        $user = Auth::user();
        $requests = Requests::where('request_initiator', $user->id)->orderBy('created_at', 'desc')->paginate(10);
        $requests = json_decode(json_encode($requests))->data;

        return response()->json($requests, 200);

    }

    public function proccessLevelsRoles($requests_id)
    {
        $requests = Requests::where('id', $requests_id)->with([
            'process' => function ($q) {
                return $q->with('levels')->get();
            }
        ])->first();
        return response()->json($requests->process->levels, 200);
    }


}
