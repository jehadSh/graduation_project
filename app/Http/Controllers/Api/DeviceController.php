<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Device;
use Illuminate\Http\Request;

class DeviceController extends Controller
{

    public function index()
    {
        $device = Device::where('id', ">", 0)->get();
        return response()->json($device, 200);
    }


    public function store(Request $request)
    {
        // Device::create($request->all());
        $device = new Device();
        $device->name = $request->name;
        $device->quantity = $request->quantity;
        $device->category = $request->category;
        $device->user_id = $request->user_id;
        $device->parts = json_encode($request->parts);
        $device->history = $request->history;
        $device->save();

        return response()->json("Done Create", 201);

    }


    public function show($id)
    {
        $device = Device::where('id', $id)->first();
        return response()->json($device, 200);

    }

    public function update(Request $request, $id)
    {
        $device = Device::where('id', $id)->first();
        $device->update($request->all());

        return response()->json($device, 200);

    }

    public function destroy($id)
    {
        $device = Device::where('id', $id)->delete();
        if (!$device) {
            return response()->json('It does not exist actually', 200);
        }
        // $this->authorize('delete', $education);
        return response()->json('Done Delete Form', 200);

    }
}