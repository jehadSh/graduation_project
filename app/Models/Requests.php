<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Requests extends Model
{
    use HasFactory;

    protected $table = 'requests';

    protected $appends = ['initiator_name', 'process_name'];

    protected $guarded = [];

    public function process()
    {
        return $this->belongsTo(Process::class, 'request_process');
    }

    public function activeLevel()
    {
        return $this->hasMany(ActiveLevel::class, 'request_id');
    }

    public function valueForm()
    {
        return $this->belongsTo(ValueForm::class, 'initiator_form_value');

    }

    public function user()
    {
        return $this->belongsTo(User::class, 'request_initiator');
    }

    public function getInitiatorNameAttribute()
    {
        return $this->user()->pluck('name')->first();
    }

    public function getProcessNameAttribute()
    {
        return $this->process()->pluck('name')->first();
    }
}
