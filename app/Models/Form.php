<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function levels()
    {
        return $this->hasMany(Level::class, 'form_id');
    }

    public function fields()
    {
        return $this->hasMany(Field::class, 'form_id');
    }
    public function processes()
    {
        return $this->hasMany(Process::class, 'initiator_form_id');
    }

}