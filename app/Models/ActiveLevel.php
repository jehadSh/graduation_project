<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ActiveLevel extends Model
{
    use HasFactory;

    protected $appends = ['Username'];

    protected $guarded = [];

    public function request()
    {
        return $this->belongsTo(Requests::class, 'request_id');
    }

    public function level()
    {
        return $this->belongsTo(Level::class, 'level_id');
    }

    public function ValueFormLevel()
    {
        return $this->belongsTo(ValueForm::class, 'valueForm_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getUserNameAttribute()
    {
        return $this->user()->pluck('name')->first();
    }

}
