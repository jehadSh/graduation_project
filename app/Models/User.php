<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use Notifiable;

    protected $appends = ['roles_value'];

    protected $guarded = [];
    protected $hidden = ['password'];


    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_user');
    }

    public function requests(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Requests::class, 'request_initiator');
    }

    public function activeLevels(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(ActiveLevel::class, 'user_id');
    }

    public function getRolesValueAttribute()
    {
        return $this->roles()->pluck('name');
    }
    public function inPermissions($policy)
    {
        foreach ($this->roles as $role) {
            foreach ($role->permissions as $permission) {
                if ($permission->name == $policy) {
                    return true;
                }
            }
        }
        return false;
    }
}
