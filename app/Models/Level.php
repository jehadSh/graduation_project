<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    use HasFactory;

    protected $appends = ['role_name', 'form_name'];

    protected $guarded = [];

    public function form()
    {
        return $this->belongsTo(Form::class, 'form_id');
    }

    public function proces()
    {
        return $this->belongsTo(Proces::class, 'process_id');
    }

    public function ActiveLevel()
    {
        return $this->hasMany(ActiveLevel::class, 'level_id');
    }

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }

    public function getRoleNameAttribute()
    {
        return $this->role()->pluck('name')->first();
    }

    public function getFormNameAttribute()
    {
        return $this->form()->pluck('name')->first();
    }



}
