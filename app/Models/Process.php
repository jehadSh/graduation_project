<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Process extends Model
{
    use HasFactory;

    protected $table = 'process';

    protected $guarded = [];


    public function levels()
    {
        return $this->hasMany(Level::class, 'process_id');
    }

    public function form()
    {
        return $this->belongsTo(Form::class, 'initiator_form_id');
    }

    public function requests()
    {
        return $this->hasMany(Requests::class, 'request_process');
    }

}