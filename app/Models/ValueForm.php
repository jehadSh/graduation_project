<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ValueForm extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function fieldAnswers()
    {
        return $this->hasMany(FieldAnswer::class, 'valueForm_id');
    }

    public function ActiveLevel()
    {
        return $this->hasOne(ActiveLevel::class, 'valueForm_id');
    }

    public function request()
    {
        return $this->hasOne(Requests::class,'initiator_form_value');

    }
}
