<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FieldAnswer extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function form()
    {
        return $this->belongsTo(ValueForm::class, 'valueForm_id');
    }

    public function field()
    {
        return $this->belongsTo(Field::class, 'field_id');
    }

}
