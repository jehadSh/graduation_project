<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function form()
    {
        return $this->belongsTo(Form::class, 'form_id');
    }

    public function collections()
    {
        return $this->belongsTo(Collection::class, 'collection_id');
    }

    public function fildAnswers()
    {
        return $this->hasMany(FieldAnswer::class, 'field_id');
    }
}
