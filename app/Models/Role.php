<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;
    protected $table = 'roles';

    // protected $fillable = ['name','created_at', 'updated_at'];
    protected $guarded = [];

    public function users()
    {
        return $this->belongsToMany(User::class, 'role_user');
    }
    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'permission_role');
    }

    public function levels(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Level::class, 'role_id');
    }
}
