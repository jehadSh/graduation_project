<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Collection extends Model
{
    use HasFactory;


    protected $guarded = [];


    public function fields()
    {
        return $this->hasMany(Field::class, 'collection_id');
    }

    public function dataValues(){
        return $this->hasMany(DataValue::class,'collectoin_id');
    }
}
