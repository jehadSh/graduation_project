<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActiveLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('active_levels', function (Blueprint $table) {
            $table->id();
            $table->integer('request_id');
            $table->integer('level_id');
            $table->integer('user_id')->nullable();
            $table->integer('valueForm_id')->nullable();
            $table->boolean('is_confirmed')->nullable();
            $table->boolean('is_level_complete')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('active_levels');
    }
}
