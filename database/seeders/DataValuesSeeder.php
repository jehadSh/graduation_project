<?php

namespace Database\Seeders;

use App\Models\DataValue;
use Illuminate\Database\Seeder;

class DataValuesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataValue = [
            [
                'value'=>'KIA',
                'collectoin_id'=>1,
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12',
            ],
            [
                'value'=>'AUDI',
                'collectoin_id'=>1,
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12',
            ],
            [
                'value'=>'DMG',
                'collectoin_id'=>1,
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12',
            ],

            [
                'value'=>'Lights',
                'collectoin_id'=>2,
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12',
            ],
            [
                'value'=>'Wheels',
                'collectoin_id'=>2,
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12',
            ],
            [
                'value'=>'Mirrors',
                'collectoin_id'=>2,
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12',
            ],

            //3
            [
                'value'=>'Low',
                'collectoin_id'=>3,
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12',
            ],
            [
                'value'=>'Midum',
                'collectoin_id'=>3,
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12',
            ],
            [
                'value'=>'High',
                'collectoin_id'=>3,
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12',
            ],
           
        ];
            DataValue::insert($dataValue);
    }
}
