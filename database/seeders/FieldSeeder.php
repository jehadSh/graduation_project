<?php

namespace Database\Seeders;

use App\Models\Field;


use Illuminate\Database\Seeder;

class FieldSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $field = [
            [
                'type' => "text",
                'label' => 'Problem Summary',
                "collection_id" => 1,
                "form_id" => 2,
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12',
            ],
            [
                'type' => "choice",
                'label' => 'Car Model',
                "collection_id" => 1,
                "form_id" => 2,
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12',
            ],
            [
                'type' => "checkbox",
                'label' => 'damage level',
                "collection_id" => 3,
                "form_id" => 2,
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12',
            ],
            

            [
                'type' => "text",
                'label' => 'Costs',
                "collection_id" => 2,
                "form_id" => 3,
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12',
            ],
            [
                'type' => "choice",
                'label' => 'Fixed parts',
                "collection_id" => 2,
                "form_id" => 3,
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12',
            ],
            [
                'type' => "checkbox",
                'label' => 'damage level',
                "collection_id" => 3,
                "form_id" => 3,
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12',
            ]
        ];
        Field::insert($field);
    }
}