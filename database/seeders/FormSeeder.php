<?php

namespace Database\Seeders;

use App\Models\Form;
use Illuminate\Database\Seeder;

class FormSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $form = [
            [
                'id' => "1",
                'name' => 'no Form',
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12',
            ],
            [
                'id' => "2",
                'name' => 'Problem description',
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12',
            ],
            [
                'id' => "3",
                'name' => 'Maintenance report',
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12',
            ]
        ];
        Form::insert($form);
    }
}