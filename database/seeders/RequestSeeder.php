<?php

namespace Database\Seeders;

use App\Models\Requests;
use Illuminate\Database\Seeder;

class RequestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $request = [
            [
                'id' => "1",
                "request_initiator" => 1,
                "request_process" => 1,
                "initiator_form_value" => 1,
                "request_status" => "new",
                'created_at' => '2021-10-12',
                'updated_at' => '2021-10-12',
            ],
        ];
        Requests::insert($request);
    }
}
