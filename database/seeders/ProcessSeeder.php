<?php

namespace Database\Seeders;

use App\Models\Process;
use Illuminate\Database\Seeder;

class ProcessSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $process = [
            [
                'id' => "1",
                "name" => "Car Maintenance",
                "initiator_role" => 2,
                "initiator_form" => 2,
                "priority" => "high",
                'created_at' => '2021-10-12',
                'updated_at' => '2021-10-12',
            ],
        ];
        Process::insert($process);
    }
}
