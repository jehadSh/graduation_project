<?php

namespace Database\Seeders;

use App\Models\User_Role;
use Illuminate\Database\Seeder;

class CreateUser_RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user_role = [
            [
                'user_id' => '1',
                'role_id' => '1',
                'created_at'=>'2023-6-12',
                'updated_at'=>'2023-6-12',
            ],

            [
                'user_id' => '2',
                'role_id' => '2',
                'created_at'=>'2023-6-12',
                'updated_at'=>'2023-6-12',
            ],
            [
                'user_id' => '3',
                'role_id' => '3',
                'created_at'=>'2023-6-12',
                'updated_at'=>'2023-6-12',
            ],

        ];

        User_Role::insert($user_role);

    }
}
