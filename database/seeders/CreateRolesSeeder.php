<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class CreateRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = [
            [
                'name'=>'Super Admin',
                'description'=>'aaaa',
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12',
            ],
            [
                'name'=>'manger',
                'description'=>'aaaa',
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12',
            ],
            [
                'name'=>'Editor',
                'description'=>'aaaa',
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12',
            ],

        ];


            Role::insert($role);

    }
}
