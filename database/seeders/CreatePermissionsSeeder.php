<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;

class CreatePermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $permission = [

            // Start User //
            [
                'name' => 'viewAnyUser',
                'created_at' => '2021-10-12',
                'updated_at' => '2021-10-12',
            ],
            [
                'name' => 'createUser',
                'created_at' => '2021-10-12',
                'updated_at' => '2021-10-12',
            ],
            [
                'name' => 'viewUser',
                'created_at' => '2021-10-12',
                'updated_at' => '2021-10-12',
            ],
            [
                'name' => 'updateUser',
                'created_at' => '2021-10-12',
                'updated_at' => '2021-10-12',
            ],
            [
                'name' => 'deleteUser',
                'created_at' => '2021-10-12',
                'updated_at' => '2021-10-12',
            ],
            // End User //

            // Start Role //
            [
                'name' => 'viewAnyRole',
                'created_at' => '2021-10-12',
                'updated_at' => '2021-10-12',
            ],
            [
                'name' => 'createRole',
                'created_at' => '2021-10-12',
                'updated_at' => '2021-10-12',
            ],
            [
                'name' => 'viewRole',
                'created_at' => '2021-10-12',
                'updated_at' => '2021-10-12',
            ],
            [
                'name' => 'updateRole',
                'created_at' => '2021-10-12',
                'updated_at' => '2021-10-12',
            ],
            [
                'name' => 'deleteRole',
                'created_at' => '2021-10-12',
                'updated_at' => '2021-10-12',
            ],
            // End Role //

            // Start Form //
            [
                'name' => 'viewAnyForm',
                'created_at' => '2021-10-12',
                'updated_at' => '2021-10-12',
            ],
            [
                'name' => 'createForm',
                'created_at' => '2021-10-12',
                'updated_at' => '2021-10-12',
            ],
            [
                'name' => 'viewForm',
                'created_at' => '2021-10-12',
                'updated_at' => '2021-10-12',
            ],
            [
                'name' => 'updateForm',
                'created_at' => '2021-10-12',
                'updated_at' => '2021-10-12',
            ],
            [
                'name' => 'deleteForm',
                'created_at' => '2021-10-12',
                'updated_at' => '2021-10-12',
            ],
            // End Form //

            // Start Process //
            [
                'name' => 'viewAnyProcess',
                'created_at' => '2021-10-12',
                'updated_at' => '2021-10-12',
            ],
            [
                'name' => 'createProcess',
                'created_at' => '2021-10-12',
                'updated_at' => '2021-10-12',
            ],
            [
                'name' => 'viewProcess',
                'created_at' => '2021-10-12',
                'updated_at' => '2021-10-12',
            ],
            [
                'name' => 'updateProcess',
                'created_at' => '2021-10-12',
                'updated_at' => '2021-10-12',
            ],
            [
                'name' => 'deleteProcess',
                'created_at' => '2021-10-12',
                'updated_at' => '2021-10-12',
            ],
            // End Process //

            // Start Device //
            [
                'name' => 'viewAnyDevice',
                'created_at' => '2021-10-12',
                'updated_at' => '2021-10-12',
            ],
            [
                'name' => 'createDevice',
                'created_at' => '2021-10-12',
                'updated_at' => '2021-10-12',
            ],
            [
                'name' => 'viewDevice',
                'created_at' => '2021-10-12',
                'updated_at' => '2021-10-12',
            ],
            [
                'name' => 'updateDevice',
                'created_at' => '2021-10-12',
                'updated_at' => '2021-10-12',
            ],
            [
                'name' => 'deleteDevice',
                'created_at' => '2021-10-12',
                'updated_at' => '2021-10-12',
            ],
            // End Device //

            // Start ValueForm //
            [
                'name' => 'viewAnyValueForm',
                'created_at' => '2021-10-12',
                'updated_at' => '2021-10-12',
            ],
            [
                'name' => 'createValueForm',
                'created_at' => '2021-10-12',
                'updated_at' => '2021-10-12',
            ],
            [
                'name' => 'viewValueForm',
                'created_at' => '2021-10-12',
                'updated_at' => '2021-10-12',
            ],
            [
                'name' => 'updateValueForm',
                'created_at' => '2021-10-12',
                'updated_at' => '2021-10-12',
            ],
            [
                'name' => 'deleteValueForm',
                'created_at' => '2021-10-12',
                'updated_at' => '2021-10-12',
            ],
            // End ValueForm //

            // Start Requests //
            [
                'name' => 'viewAnyRequests',
                'created_at' => '2021-10-12',
                'updated_at' => '2021-10-12',
            ],
            [
                'name' => 'createRequests',
                'created_at' => '2021-10-12',
                'updated_at' => '2021-10-12',
            ],
            [
                'name' => 'viewRequests',
                'created_at' => '2021-10-12',
                'updated_at' => '2021-10-12',
            ],
            [
                'name' => 'updateRequests',
                'created_at' => '2021-10-12',
                'updated_at' => '2021-10-12',
            ],
            [
                'name' => 'deleteRequests',
                'created_at' => '2021-10-12',
                'updated_at' => '2021-10-12',
            ],
            // End Requests //


        ];

        Permission::insert($permission);
    }
}