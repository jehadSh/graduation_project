<?php

namespace Database\Seeders;

use App\Models\Collection;
use Illuminate\Database\Seeder;

class CollectionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $collection = [
            [
                'name' => 'Cars Models',
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12',
            ],
            [
                'name' => 'Car parts',
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12',
            ],
            [
                'name' => 'damage levels',
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12',
            ],

        ];
        Collection::insert($collection);
    }
}