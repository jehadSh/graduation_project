<?php

namespace Database\Seeders;

use App\Models\Level;
use Illuminate\Database\Seeder;

class LevelsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $level = [
            [
                "form_id" => 2,
                "comfirmable" => true,
                "role_id" => 1,
                "process_id" => 1,
                "next_level_id" => 2,
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12',
            ],
            [
                "form_id" => 3,
                "comfirmable" => true,
                "role_id" => 3,
                "process_id" => 1,
                "next_level_id" => null,
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12',
            ]
        ];
        Level::insert($level);
    }
}
