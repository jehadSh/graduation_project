<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(CreateUsersSeeder::class);
        $this->call(CreateRolesSeeder::class);
        $this->call(CreatePermissionsSeeder::class);
        $this->call(CreatePermissions_RolesSeeder::class);
        $this->call(CreateUser_RolesSeeder::class);
        $this->call(CollectionsSeeder::class);
        $this->call(DataValuesSeeder::class);
        // $this->call(FormSeeder::class);
        // $this->call(FieldSeeder::class);
        // $this->call(LevelsSeeder::class);
        // $this->call(ProcessSeeder::class);
        // $this->call(RequestSeeder::class);
    }
}
