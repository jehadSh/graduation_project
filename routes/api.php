<?php

use App\Http\Controllers\Api\CollectionController;
use App\Http\Controllers\Api\FormController;
use App\Http\Controllers\Api\ProcessController;
use App\Http\Controllers\Api\RoleController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\DeviceController;
use App\Http\Controllers\Api\RequestController;
use App\Http\Controllers\Api\ValueFormController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post("/login", [AuthController::class, 'login']);
Route::post("/is_token", [AuthController::class, 'isToken']);


Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::resource('roles', RoleController::class);
    Route::resource('forms', FormController::class);
    Route::resource('users', UserController::class);
    Route::resource('processes', ProcessController::class);
    Route::resource('collections', CollectionController::class);
    Route::resource('devices', DeviceController::class);
    Route::resource('requests', RequestController::class);
    Route::resource('value_form', ValueFormController::class);

});

Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::post("/logout", [AuthController::class, 'logout']);
    Route::get("/isToken", [AuthController::class, 'isToken']);
    Route::get("/getAllPermission", [RoleController::class, 'getAllPermission']);
    Route::get("/over_view_request", [RequestController::class, 'overViewRequest']);
});

Route::group(['middleware' => 'auth:sanctum', 'prefix' => 'process'], function () {
    Route::get("/get_my_process_init", [ProcessController::class, 'getMyProcessInit']);
    Route::get("/get_initiator_level/{id}", [ProcessController::class, 'getInitiatorLevel']);
});

Route::group(['middleware' => 'auth:sanctum', 'prefix' => 'request'], function () {
    Route::get("/received_requests", [RequestController::class, 'receivedRequests']);
    Route::get("/get_request_active_level/{id}", [RequestController::class, 'getRequestActiveLevel']);
    Route::post("/active_level_submit/{ActiveLevel_id}", [RequestController::class, 'activeLevelSubmit']);
    Route::get("/get_my_requests", [RequestController::class, 'getMyRequests']);
    Route::get("/take_requests/{requests_id}", [RequestController::class, 'takeRequests']);
    Route::get("/on_hold_received_request", [RequestController::class, 'onHoldReceivedRequest']);
    Route::get("/rejected_received_request", [RequestController::class, 'rejectedReceivedRequest']);
    Route::get("/get_my_request_by_status/{status}", [RequestController::class, 'getMyRequestByStatus']);
    Route::get("/received_request_level/{requests_id}", [RequestController::class, 'receivedRequestLevel']);
    Route::get("/my_latest_requests", [RequestController::class, 'myLatestRequests']);
    Route::get("/proccess_levels_roles/{requests_id}", [RequestController::class, 'proccessLevelsRoles']);
});
